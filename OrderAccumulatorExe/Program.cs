﻿using OrderAccumulatorApp;
using QuickFix;

string file = args[0];
SessionSettings settings = new SessionSettings(args[0]);
IApplication order = new OrderAccumulatorLib();
IMessageStoreFactory storeFactory = new FileStoreFactory(settings);
ILogFactory logFactory = new FileLogFactory(settings);
// ThreadedSocketAcceptor socketAcceptor = new ThreadedSocketAcceptor(order, storeFactory, settings, logFactory);
IAcceptor acceptor = new ThreadedSocketAcceptor(order, storeFactory, settings, logFactory);

acceptor.Start();
Console.WriteLine("press <enter> to quit");
Console.Read();
acceptor.Stop();
// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
Console.WriteLine("Finished Acceptor");
// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");
///Users/marcelamarques/OrderAccumulatorApp/spec/fix/FIX44.xml