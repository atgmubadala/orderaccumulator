using Bogus;
using OrderAccumulatorApp.Services;
using QuickFix.Fields;
using QuickFix.FIX44;

namespace OrderAccumulatorTest
{
    public class OrderTest
    {
        [Fact]
        public void OrderRecivedTest()
        {
            ICalculation financial = new FinancialExposure(new Symbol("SymbolTest"));
            IServices serviceFin = new FinancialService(financial);
            IEnumerable<NewOrderSingle> orders = new List<NewOrderSingle>{
                CreateOrder(0, "SymbolTest"),
                CreateOrder(1, "SymbolTest"),
                CreateOrder(2, "SymbolTest"),
                CreateOrder(3, "SymbolTest"),
                CreateOrder(4, "SymbolTest2"),
                CreateOrder(5, "SymbolTest2"),
                CreateOrder(6, "SymbolTest2"),
                CreateOrder(7, "SymbolTest2"),
                CreateOrder(8, "SymbolTest2"),
                CreateOrder(9, "SymbolTest2"),
            };

            Assert.True(!String.Equals(orders.ToList()[0].Account.getValue(), orders.ToList()[1].Account.getValue()));
            decimal expessure = serviceFin.Execute(orders);
            Assert.True(expessure > 0);
        }

        private  NewOrderSingle CreateOrder(int seed, string symbol)
        {
            Random random = new Random(seed);
            IEnumerable<string> symbols = new List<string> { "PETR4", "VALE3", "VIIA4" };
            IEnumerable<char> sides = new List<char> { Side.BUY, Side.SELL};

            Faker<ClOrdID> fakeClOrdID = new Faker<ClOrdID>("pt_BR")
                .RuleFor(o => o.Obj, f => f.Random.Word());
            Faker<Symbol> fakeSymbol = new Faker<Symbol>("pt_BR")
                .RuleFor(o => o.Obj, f => f.Random.Word());
            Faker<Price> fakePrice = new Faker<Price>("pt_BR")
                .RuleFor(o => o.Obj, f => decimal.Round(f.Random.Decimal((decimal)0.01, 1000), 2, MidpointRounding.AwayFromZero));
            Faker<Account> fakeAccount = new Faker<Account>("pt_BR")
                .RuleFor(o => o.Obj, f => f.Random.Word());
            Faker<OrderQty> fakeOrderQty = new Faker<OrderQty>("pt_BR")
                .RuleFor(o => o.Obj, f => f.Random.Int(0, 1000));

            Faker<Side> fakeSide = new Faker<Side>("pt_BR")
                .RuleFor(o => o.Obj, f => f.PickRandom(sides));


            Faker<NewOrderSingle> order = new Faker<NewOrderSingle>("pt_BR")
                .RuleFor(o => o.ClOrdID, fakeClOrdID.UseSeed(random.Next()).Generate())
                .RuleFor(o => o.Symbol, new Symbol(symbol))
                .RuleFor(o => o.Side, fakeSide.UseSeed(seed).Generate())
                .RuleFor(o => o.TransactTime, new TransactTime(DateTime.Now))
                .RuleFor(o => o.OrdType, f => new OrdType(OrdType.LIMIT))
                .RuleFor(o => o.OrderQty, f => fakeOrderQty.Generate())
                .RuleFor(o => o.Price, fakePrice.UseSeed(random.Next()).Generate())
                .RuleFor(o => o.Account, fakeAccount.UseSeed(random.Next()).Generate());
            
            return order.Generate();
        }
    }
}