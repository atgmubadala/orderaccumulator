using System.Runtime.InteropServices.ComTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuickFix.Fields;
using QuickFix.FIX44;

namespace OrderAccumulatorApp.Services
{
    public class FinancialExposure : ICalculation
    {
        public Symbol Symbol { get; set; }
        public FinancialExposure(Symbol symbol)
        {
            Symbol = symbol;
        }
        public decimal Calcule(IEnumerable<NewOrderSingle> orders)
        {
            List<NewOrderSingle> buy = orders.Where(w => char.Equals(w.Side.getValue(), Side.BUY)).ToList();
            List<NewOrderSingle> sell = orders.Where(w => char.Equals(w.Side.getValue(), Side.SELL)).ToList();
            decimal decbuy = SumOrder(orders, Side.BUY);
            decimal decsell = SumOrder(orders, Side.SELL);
            IEnumerable<NewOrderSingle> ordersBySymbols = GetBySymbol(orders, Symbol.getValue());
            return SumOrder(ordersBySymbols, Side.BUY) - SumOrder(ordersBySymbols, Side.SELL);
        }

        private decimal SumOrder(IEnumerable<NewOrderSingle> orders, char side)
        {
            return orders.ToList().Where(
                            w => char.Equals(w.Side.getValue(), side)).ToList().Sum(
                                s => s.Price.getValue() * s.OrderQty.getValue());
        }

        private IEnumerable<NewOrderSingle> GetBySymbol(IEnumerable<NewOrderSingle> orders, string symbol){
            return orders.Where(w => String.Equals(w.Symbol.getValue(), symbol));
        }
    }
}