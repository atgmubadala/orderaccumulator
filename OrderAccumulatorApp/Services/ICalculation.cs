using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuickFix.FIX44;

namespace OrderAccumulatorApp.Services
{
    public interface ICalculation
    {
        decimal Calcule(IEnumerable<NewOrderSingle> orders);
    }
}