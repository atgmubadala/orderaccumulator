using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuickFix.FIX44;

namespace OrderAccumulatorApp.Services
{
    public class FinancialService : IServices
    {
        public ICalculation Calculation { get; set ; }

        public FinancialService(ICalculation calculation)
        {
            Calculation = calculation;
        }
        public decimal Execute(IEnumerable<NewOrderSingle> orders)
        {
            return Calculation.Calcule(orders);
        }
    }
}