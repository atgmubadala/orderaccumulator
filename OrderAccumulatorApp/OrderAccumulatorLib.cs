using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuickFix;
using QuickFix.Fields;
using QuickFix.FIX44;
using Message = QuickFix.Message;

namespace OrderAccumulatorApp
{
    public class OrderAccumulatorLib : IApplication
    {
        static readonly decimal DEFAULT_MAX_PRICE = 1000;

        int orderID = 0;
        int execID = 0;

        private string GenOrderID() { return (++orderID).ToString(); }
        private string GenExecID() { return (++execID).ToString(); }
        
        public void FromAdmin(Message message, SessionID sessionID)
        {
            
        }

        public void FromApp(Message message, SessionID sessionID)
        {
            
        }

        public void OnCreate(SessionID sessionID)
        {
            
        }

        public void OnLogon(SessionID sessionID)
        {
            
        }

        public void OnLogout(SessionID sessionID)
        {
            
        }

        public void ToAdmin(Message message, SessionID sessionID)
        {
            
        }

        public void ToApp(Message message, SessionID sessionId)
        {
            
        }

        public void onMessage(NewOrderSingle n, SessionID s)
        {
            if(n.Price.getValue() < DEFAULT_MAX_PRICE){
                MessageSucess(n, s);
            }
            // Falta código para add a messgem ao repositório e assim entrar no calculo da Exposição Financieira
            // Falta o código de rejeição
        }

        private void MessageSucess(NewOrderSingle n, SessionID s)
        {
            Symbol symbol = n.Symbol;
            Side side = n.Side;
            OrdType ordType = n.OrdType;
            OrderQty orderQty = n.OrderQty;
            Price price = new Price(DEFAULT_MAX_PRICE);
            ClOrdID clOrdID = n.ClOrdID;

            QuickFix.FIX44.ExecutionReport exReport = new QuickFix.FIX44.ExecutionReport(
                            new OrderID(GenOrderID()),
                            new ExecID(GenExecID()),
                            new ExecType(ExecType.FILL),
                            new OrdStatus(OrdStatus.FILLED),
                            symbol, //shouldn't be here?
                            side,
                            new LeavesQty(0),
                            new CumQty(orderQty.getValue()),
                            new AvgPx(price.getValue()));

            exReport.Set(clOrdID);
            exReport.Set(symbol);
            exReport.Set(orderQty);
            exReport.Set(new LastQty(orderQty.getValue()));
            exReport.Set(new LastPx(price.getValue()));

            if (n.IsSetAccount())
                exReport.SetField(n.Account);

            try
            {
                Session.SendToTarget(exReport, s);
            }
            catch (SessionNotFound ex)
            {
                Console.WriteLine("==session not found exception!==");
                Console.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}